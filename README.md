README
==================
This program implements the 3DMMS algorithm proposed in 3DMMS: from nucleus to membrane morphological segmentation. 


******

|Author|Jianfeng CAO|
|---|---
|E-mail|jfcao3-c@my.cityu.edu.hk

*****
## Usage
* **Platform dependency**
	
	This program is written with Matlab 2017b.

* **Data preparation**

* **Parameters settting**
	Example dataset is included in . If you want to analyze your own dataset, you might need to change parameters
	according to your own dataset. `DTWatershed` function includs all the parameters you need to tune. They are
	 listed as following:
	 
	 |Parameter name|Meaning|
	 |----|----|
	 |data_name|DataSet name of the embrayo image|
	 |max_Time|The maximal time point of the embryo stack|
	 |prescale|The downsample ratio of each slice|
	 |reduceRatio|The ratio of downsampling on the whole embryo|
	 |xy_resolution|The resolution on each slice|
	 |z_resolution|The distance of each slices|